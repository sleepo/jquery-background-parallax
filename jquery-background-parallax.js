(function( $ ) {
    $.fn.bgParallax = function() {

        var BgParallax = function($el){
            var that = this;
            this.$el = $el;

            this.isInViewport = function(cb){

                var offsetTop = that.$el.offset().top,
                    offsetBottom = that.$el.height() + offsetTop;

                var windowOffsetTop = $(window).scrollTop(),
                    screenHeight = $(window).height();

                if ( windowOffsetTop + screenHeight >= offsetTop &&
                    windowOffsetTop <= offsetBottom ){

                    var currentlyView = windowOffsetTop + screenHeight - offsetTop,
                        percentView = currentlyView / (offsetBottom - offsetTop + screenHeight);

                    cb(percentView);

                }

            };

            this.getSizes = function(cb){
                var image_url = that.$el.css('background-image'),
                    image;

                image_url = image_url.match(/^url\("?(.+?)"?\)$/);

                if (image_url[1]) {
                    image_url = image_url[1];
                    image = new Image();

                    $(image).load(function () {

                        var sizes = {
                            viewport: {
                                w: that.$el.width(),
                                h: that.$el.height()
                            },
                            image: {
                                w: image.width,
                                h: image.height
                            }
                        };

                        if ( image.width != that.$el.width() ){
                            var newWidth = that.$el.width(),
                                newHeight = newWidth * image.height / image.width;

                            sizes.image = {
                                w: newWidth,
                                h: newHeight
                            };
                        }

                        if (cb){
                            cb(sizes);
                        }
                    });

                    image.src = image_url;
                }
            };

            this.lastBgPosition = null;
            this.init = function(){
                that.getSizes(function(sizes){
                    $(window).on('scroll', function(){
                        that.isInViewport(function(percent){
                            var possibleOffset = sizes.image.h - sizes.viewport.h;
                            var offset = -(possibleOffset * percent);
                            if (offset > 0){
                                console.log('Ooos, img is to small, no parallax effect');
                            } else {
                                if ( that.lastBgPosition != offset ){
                                    that.lastBgPosition = offset;
                                    var bgStyle = '50% ' + offset + 'px';
                                    that.$el.css({'background-position': bgStyle});
                                }
                            }
                        });
                    });
                });
            };
            this.init();
        }

        return this.each(function(index, el){
            new BgParallax( $(el) );
        });
    };
})(jQuery);