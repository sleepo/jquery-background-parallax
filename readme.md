#jQuery Background Parallax plugin
jQuery plugin that allow to create parallax effect.

## How start
Create element and specify background-image, note that element height must be less than picture height.

```
#!javascript
$('.parallax').bgParallax();
```